package com.mdmatrakas.calcrestapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class CalcActivity extends AppCompatActivity {
    private String LOG_TAG = "CalcActivity";

    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";

    // Se a conexão Wi-Fi está disponível
    private static boolean wifiConnected = false;
    // Se a conexão de dados móveis (3G/4G) está disponível
    private static boolean mobileConnected = false;
    // Se o display deve ser atualizado
    public static boolean refreshDisplay = true;
    public static String sPref = null;

    // O BroadcastReceiver que monitora mudanças nas conexões de rede
    private NetworkReceiver receiver = new NetworkReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);

        // Registra o BroadcastReceiver para acompanhar mudanças nas conexões de rede
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkReceiver();
        this.registerReceiver(receiver, filter);

        // Carrega as preferencias do usuário quanto ao tipo de conexão a ser utilizada.
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Carrega um valor do arquivo de preferencias. O segundo parametro é o valor padrão caso
        // uma variável de preferencia não seja encontrada.
        sPref = sharedPrefs.getString("listPref", "Wi-Fi");

        updateConnectedFlags();

        Button btnSoma = (Button) findViewById(R.id.buttonSoma);
        btnSoma.setOnClickListener(new ClickList(1));
        Button btnSub = (Button) findViewById(R.id.buttonSub);
        btnSub.setOnClickListener(new ClickList(2));
        Button btnMul = (Button) findViewById(R.id.buttonMul);
        btnMul.setOnClickListener(new ClickList(3));
        Button btnDiv = (Button) findViewById(R.id.buttonDiv);
        btnDiv.setOnClickListener(new ClickList(4));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(receiver);
    }

    // Verifica a conexão com a rede e seta os atributos wifiConnected and mobileConnected de acordo.
    private void updateConnectedFlags() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        } else {
            wifiConnected = false;
            mobileConnected = false;
        }

        Log.i(LOG_TAG, "wi-fi: " + wifiConnected + " mobile:" + mobileConnected);
    }

    private class ClickList implements View.OnClickListener {

        private int oper;

        public ClickList(int oper) {
            this.oper = oper;
        }

        @Override
        public void onClick(View v) {

            String params[] = new String[3];
            params[1] = ((EditText) findViewById(R.id.editValA)).getText().toString();
            params[2] = ((EditText) findViewById(R.id.editValB)).getText().toString();

            switch (oper) {
                case 1:
                    params[0] = "soma";
                    break;
                case 2:
                    params[0] = "subtracao";
                    break;
                case 3:
                    params[0] = "multiplicacao";
                    break;
                case 4:
                    params[0] = "divisao";
                    break;
            }

            if((sPref.equals(ANY)) && (wifiConnected || mobileConnected)) {
                Log.i(LOG_TAG, "wifi || mobile");
                new SolicitarOperacaoTask().execute(params);
            }
            else if ((sPref.equals(WIFI)) && (wifiConnected)) {
                Log.i(LOG_TAG, "wifi");
                new SolicitarOperacaoTask().execute(params);
            } else {
                // Mostrar erro
                Log.i(LOG_TAG, "sem conexão");
                new SolicitarOperacaoTask().execute(params);
            }
        }
    }

    /**
     *
     * This BroadcastReceiver intercepts the android.net.ConnectivityManager.CONNECTIVITY_ACTION,
     * which indicates a connection change. It checks whether the type is TYPE_WIFI.
     * If it is, it checks whether Wi-Fi is connected and sets the wifiConnected flag in the
     * main activity accordingly.
     *
     */
    public class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connMgr =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            // Checks the user prefs and the network connection. Based on the result, decides
            // whether
            // to refresh the display or keep the current display.
            // If the userpref is Wi-Fi only, checks to see if the device has a Wi-Fi connection.
            if (WIFI.equals(sPref) && networkInfo != null
                    && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // If device has its Wi-Fi connection, sets refreshDisplay
                // to true. This causes the display to be refreshed when the user
                // returns to the app.
                refreshDisplay = true;
                Toast.makeText(context, "Wi-Fi conectado", Toast.LENGTH_SHORT).show();

                // If the setting is ANY network and there is a network connection
                // (which by process of elimination would be mobile), sets refreshDisplay to true.
            } else if (ANY.equals(sPref) && networkInfo != null) {
                refreshDisplay = true;

                // Otherwise, the app can't download content--either because there is no network
                // connection (mobile or Wi-Fi), or because the pref setting is WIFI, and there
                // is no Wi-Fi connection.
                // Sets refreshDisplay to false.
            } else {
                refreshDisplay = false;
                Toast.makeText(context, "Sem conexão", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SolicitarOperacaoTask extends AsyncTask<String, Void, Operacao> {
        private String LOG_TAG = "OpTask";

         protected void onPostExecute(Operacao op) {
             if(op==null)return;
            ((TextView) findViewById(R.id.editRes)).setText(String.valueOf(op.getResultado()));
            ((TextView) findViewById(R.id.editResposta)).setText(op.toString());
        }

        @Override
        protected Operacao doInBackground(String... params) {
            String end = "http://192.168.1.16:8080/CalculadoraService";

            String url = new String(end + "/" + params[0] + "/" + params[1] + "/" + params[2]);
            Log.i(LOG_TAG, "URL: " + url);

            try {
                return loadXmlFromNetwork(url);
            } catch (IOException e) {
                return null; //Erro de conexão
            } catch (XmlPullParserException e) {
                return null; //XML mal formado
            }

        }

        // Carrega o XML a partir do servirdor e realiza a sua interpretação (parse)
        private Operacao loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
            InputStream stream = null;
            // Inicializar o parser
            OperacaoXmlReader operacaoXmlParser = new OperacaoXmlReader();
            Operacao operacao = null;

            try {
                stream = downloadUrl(urlString);

                if(stream != null)
                    operacao = operacaoXmlParser.parse(stream);

            } finally {
                // Garante que o stream de entrada seja fechado após o seu uso.
                if (stream != null) {
                    stream.close();
                }
            }

            return operacao;
        }

        // Given a string representation of a URL, sets up a connection and gets an input stream.
        private InputStream downloadUrl(String urlString) throws IOException {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(1000 /* milliseconds */);
            urlConnection.setConnectTimeout(1500 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);

            // realiza a solicitação ao servidor
            urlConnection.connect();

            int respCode = urlConnection.getResponseCode();

            Looper.prepare();

            switch(respCode)
            {
                case HttpURLConnection.HTTP_OK: //200
                return urlConnection.getInputStream();
                    case HttpURLConnection.HTTP_BAD_REQUEST: //400
                        Toast.makeText(CalcActivity.this, "Requisição ruim - não compreendida", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_UNAUTHORIZED: //401
                        Toast.makeText(CalcActivity.this, "Requisição não autorizada", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_PAYMENT_REQUIRED: //402
                        Toast.makeText(CalcActivity.this, "Pagamento necessário", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_FORBIDDEN: // 403
                        Toast.makeText(CalcActivity.this, "Proibido - acesso negado", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_NOT_FOUND: { // 404
                        Toast.makeText(CalcActivity.this, "Recurso não disponível", Toast.LENGTH_SHORT).show();
                        InputStream error = urlConnection.getErrorStream();
                        Scanner sc = new Scanner(error);
                        StringBuffer buf = new StringBuffer();
                        while (sc.hasNext())
                            buf.append(sc.next());
                        Log.i(LOG_TAG, buf.toString());
                        break;
                    }
                    case HttpURLConnection.HTTP_BAD_METHOD: // 405
                        Toast.makeText(CalcActivity.this, "Método não permitido", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_NOT_ACCEPTABLE: // 406
                        Toast.makeText(CalcActivity.this, "Não aceitável", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_PROXY_AUTH: // 407
                        Toast.makeText(CalcActivity.this, "Autenticação de proxy necessária", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_CLIENT_TIMEOUT: // 408
                        Toast.makeText(CalcActivity.this, "Tempo limite esgotado para a requisição", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_CONFLICT: // 409
                        Toast.makeText(CalcActivity.this, "Conflito", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_GONE: // 410
                        Toast.makeText(CalcActivity.this, "Recurso não mais disponível", Toast.LENGTH_SHORT).show();
                        break;
                    case HttpURLConnection.HTTP_INTERNAL_ERROR: // 500
                        Toast.makeText(CalcActivity.this, "Error interno do servidor", Toast.LENGTH_SHORT).show();
                        break;
                }
                return null;
        }
    }
}
