package com.mdmatrakas.calcrestapp;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Criado por MDMatrakas em 18/05/2017.
 */
public class OperacaoXmlReader {
    private String LOG_TAG = "XmlReader";

    // Não são usados namespaces
    private static final String ns = null;

    public Operacao parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return lerOperacao(parser);
        } finally {
            in.close();
        }
    }

    // Faz o parse do conteúdo de uma entidade XML do tipo <OPERACAO>. Ao encontrar as tags:
    // NOME, OPERADOR, OPERANDO1, OPERANDO2, RESULTADO e TIPO, chama seus respectivos métodos de
    // leitura para processamento. Caso contrário, ignora a tag.
    private Operacao lerOperacao(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "OPERACAO");
        String nome = null;
        String operador = null;
        float op1 = 0F;
        float op2 = 0F;
        float resultado = 0F;
        String tipo = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            if (tag.equals("NOME")) {
                nome = lerNome(parser);
                Log.i(LOG_TAG, "Nome: " + nome);
            } else if (tag.equals("OPERADOR")) {
                operador = lerOperador(parser);
                Log.i(LOG_TAG, "Operador: " + operador);
            } else if (tag.equals("OPERANDO1")) {
                op1 = lerOperando1(parser);
                Log.i(LOG_TAG, "Operando1: " + op1);
            } else if (tag.equals("OPERANDO2")) {
                op2 = lerOperando2(parser);
                Log.i(LOG_TAG, "Operando2: " + op2);
            } else if (tag.equals("RESULTADO")) {
                resultado = lerResultado(parser);
                Log.i(LOG_TAG, "Resultado: " + resultado);
            } else if (tag.equals("TIPO_DADO")) {
                tipo = lerTipo(parser);
                Log.i(LOG_TAG, "Tipo: " + tipo);
            } else {
                skip(parser);
                Log.i(LOG_TAG, "dessconsiderando: " + tag);
            }
        }
        return new Operacao(nome, operador, op1, op1, resultado, tipo);
    }

    private String lerNome(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "NOME");
        String nome = lerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, "NOME");
        return nome;
    }

    private String lerOperador(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "OPERADOR");
        String op = lerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, "OPERADOR");
        return op;
    }

    private float lerOperando1(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "OPERANDO1");
        float val = lerFloat(parser);
        parser.require(XmlPullParser.END_TAG, ns, "OPERANDO1");
        return val;
    }

    private float lerOperando2(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "OPERANDO2");
        float val = lerFloat(parser);
        parser.require(XmlPullParser.END_TAG, ns, "OPERANDO2");
        return val;
    }

    private float lerResultado(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "RESULTADO");
        float res = lerFloat(parser);
        parser.require(XmlPullParser.END_TAG, ns, "RESULTADO");
        return res;
    }

    private String lerTipo(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "TIPO_DADO");
        String tipo = lerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, "TIPO_DADO");
        return tipo;
    }

    private String lerTexto(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            Log.i(LOG_TAG, "xmlText: " + result);
            parser.nextTag();
        }
        return result;
    }

    private float lerFloat(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            Log.i(LOG_TAG, "xmlText: " + result);
            parser.nextTag();
        }
        return Float.parseFloat(result);
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
