package com.mdmatrakas.calcrestapp1;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;


/**
 * Criado por MDMatrakas em 18/05/2017.
 */

public class CalculadoraRestServiceAccess {
    private static String LOG_TAG = "CalcRest";
    private static String url = "http://192.168.1.16:8080/CalculadoraService";

    Context context;

    private String resp;
    private float valA;
    private float valB;

    public CalculadoraRestServiceAccess(Context c) {
        context = c;
    }

    public void setValA(float valA) {
        this.valA = valA;
    }

    public void setValB(float valB) {
        this.valB = valB;
    }

    public float getValA() {
        return valA;
    }

    public float getValB() {
        return valB;
    }

    public String getResp() {
        return resp;
    }

    public float getSoma() {
        try {
            URL url = new URL(this.url + "/soma/" + valA + "/" + valB);
            return processar(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return 0F;
    }

    public float getSubtracao() {
        try {
            URL url = new URL(this.url + "/subtracao/" + valA + "/" + valB);
            return processar(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return 0F;
    }

    public float getMultiplicacao() {
        try {
            URL url = new URL(this.url + "/multiplicacao/" + valA + "/" + valB);
            return processar(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return 0F;
    }

    public float getDivisao() {
        try {
            URL url = new URL(this.url + "/divisao/" + valA + "/" + valB);
            return processar(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return 0F;
    }

    private float processar(URL url) {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("accept", "application/xml");
            urlConnection.setDoInput(true);

            // realiza a solicitação ao servidor
            urlConnection.connect();

            int respCode = urlConnection.getResponseCode();

            switch(respCode)
            {
                case HttpURLConnection.HTTP_OK: { //200
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    Scanner sc = new Scanner(in);
                    StringBuffer buf = new StringBuffer();
                    while (sc.hasNext())
                        buf.append(sc.next());

                    resp = buf.toString();

                    Log.i(LOG_TAG, "Resposta: " + resp);
                    String val = resp.substring(resp.indexOf("<RESULTADO>") + 11, resp.indexOf("</RESULTADO>"));
                    Log.i(LOG_TAG, "Resultado: " + val);
                    if (!val.isEmpty())
                        return Float.parseFloat(val);
                    break;
                }
                case HttpURLConnection.HTTP_BAD_REQUEST: //400
                    Toast.makeText(context, "Requisição ruim - não compreendida", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED: //401
                    Toast.makeText(context, "Requisição não autorizada", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_PAYMENT_REQUIRED: //402
                    Toast.makeText(context, "Pagamento necessário", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_FORBIDDEN: // 403
                    Toast.makeText(context, "Proibido - acesso negado", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_NOT_FOUND: { // 404
                    Toast.makeText(context, "Recurso não disponível", Toast.LENGTH_SHORT).show();
                    InputStream error = urlConnection.getErrorStream();
                    Scanner sc = new Scanner(error);
                    StringBuffer buf = new StringBuffer();
                    while (sc.hasNext())
                        buf.append(sc.next());
                    Log.i(LOG_TAG, buf.toString());
                    break;
                }
                case HttpURLConnection.HTTP_BAD_METHOD: // 405
                    Toast.makeText(context, "Método não permitido", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_NOT_ACCEPTABLE: // 406
                    Toast.makeText(context, "Não aceitável", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_PROXY_AUTH: // 407
                    Toast.makeText(context, "Autenticação de proxy necessária", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_CLIENT_TIMEOUT: // 408
                    Toast.makeText(context, "Tempo limite esgotado para a requisição", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_CONFLICT: // 409
                    Toast.makeText(context, "Conflito", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_GONE: // 410
                    Toast.makeText(context, "Recurso não mais disponível", Toast.LENGTH_SHORT).show();
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR: // 500
                    Toast.makeText(context, "Error interno do servidor", Toast.LENGTH_SHORT).show();
                    break;
            }

        } catch (FileNotFoundException e) {
            InputStream error = urlConnection.getErrorStream();
            Scanner sc = new Scanner(error);
            StringBuffer buf = new StringBuffer();
            while (sc.hasNext())
                buf.append(sc.next());
            Log.i(LOG_TAG, buf.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return 0F;
    }

}

