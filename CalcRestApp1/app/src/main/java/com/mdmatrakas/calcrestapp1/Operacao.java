package com.mdmatrakas.calcrestapp1;

/**
 * Criado por MDMatrakas em 18/05/2017.
 */

//@XmlRootElement(name = "OPERACAO")
public class Operacao {
    private String nome;
    private String operador;
    private float operando1;
    private float operando2;
    private float resultado;
    private String tipo;

    @Override
    public String toString() {
        return "Operacao{\n" +
                "nome='" + nome + "\',\n" +
                "operador='" + operador + "\',\n" +
                "operando1=" + operando1 + ",\n" +
                "operando2=" + operando2 + ",\n" +
                "resultado=" + resultado + ",\n" +
                "tipo='" + tipo + '\'' +
                '}';
    }

    public Operacao() {
    }
    public Operacao(String n, String oper, float a, float b, float r, String t) {
        nome = n;
        operador = oper;
        operando1 = a;
        operando2 = b;
        resultado = r;
        tipo = t;
    }

    //@XmlElement
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    //@XmlElement
    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    //@XmlElement
    public float getOperando1() {
        return operando1;
    }

    public void setOperando1(float operando1) {
        this.operando1 = operando1;
    }

    //@XmlElement
    public float getOperando2() {
        return operando2;
    }

    public void setOperando2(float operando2) {
        this.operando2 = operando2;
    }

    //@XmlElement
    public float getResultado() {
        return resultado;
    }

    public void setResultado(float resultado) {
        this.resultado = resultado;
    }

    //@XmlElement
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
