package com.mdmatrakas.calcrestapp1;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalcActivity extends AppCompatActivity {
    private String LOG_TAG = "CalcActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);

        // Para utilizar a chamada à classe CalculadoraRestServiceAccess
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Button btnSoma = (Button) findViewById(R.id.buttonSoma);
        btnSoma.setOnClickListener(new ClickList(1));
        Button btnSub = (Button) findViewById(R.id.buttonSub);
        btnSub.setOnClickListener(new ClickList(2));
        Button btnMul = (Button) findViewById(R.id.buttonMul);
        btnMul.setOnClickListener(new ClickList(3));
        Button btnDiv = (Button) findViewById(R.id.buttonDiv);
        btnDiv.setOnClickListener(new ClickList(4));

    }


    private class ClickList implements View.OnClickListener {

        private int oper;

        public ClickList(int oper) {
            this.oper = oper;
        }

        @Override
        public void onClick(View v) {
            float valA = Float.parseFloat(((EditText) findViewById(R.id.editValA)).getText().toString());
            float valB = Float.parseFloat(((EditText) findViewById(R.id.editValB)).getText().toString());

            CalculadoraRestServiceAccess service = new CalculadoraRestServiceAccess(CalcActivity.this);

            service.setValA(valA);
            service.setValB(valB);

            float val = 0;

            switch (oper) {
                case 1:
                    val = service.getSoma();
                    break;
                case 2:
                    val = service.getSubtracao();
                    break;
                case 3:
                    val = service.getMultiplicacao();
                    break;
                case 4:
                    val = service.getDivisao();
                    break;
            }

            ((TextView) findViewById(R.id.editRes)).setText(String.valueOf(val));
            ((TextView) findViewById(R.id.editResposta)).setText(service.getResp());
        }
    }


}
