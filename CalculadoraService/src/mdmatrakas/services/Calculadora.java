package mdmatrakas.services;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class Calculadora {

	private static final String[] OPTIONS = { "OPTIONS" };
	
	private static final String SOMA_I_PATH = "/int/soma";
	private static final String SUBTRACAO_I_PATH = "/int/subtracao";
	private static final String MULTIPLICACAO_I_PATH = "/int/multiplicacao";
	private static final String DIVISAO_I_PATH = "/int/divisao";
	private static final String RESTO_I_PATH = "/int/resto";
	private static final String SOMA_PATH = "/soma";
	private static final String SUBTRACAO_PATH = "/subtracao";
	private static final String MULTIPLICACAO_PATH = "/multiplicacao";
	private static final String DIVISAO_PATH = "/divisao";
	private static final String RESTO_PATH = "/resto";

	@OPTIONS
	@Produces(MediaType.APPLICATION_XML)
	public String getXmlSupportedOperations() {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERATIONS>");
		for (String s : OPTIONS)
			xml.append("<OPERATION>").append(s).append("</OPERATION>");
		xml.append("</OPERATIONS>");
		return xml.toString();
	}
	
	@OPTIONS
	@Produces(MediaType.TEXT_PLAIN)
	public String getTextSupportedOperations() {
		StringBuffer text = new StringBuffer();
		for (String s : OPTIONS)
			text.append(s).append("\n");
		return text.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(SOMA_I_PATH + "/{valA}" + "/{valB}")
	public String getXmlSoma(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("+").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA+valB).append("</RESULTADO>");
		xml.append("<NOME>").append("soma").append("</NOME>");
		xml.append("<TIPO_DADO>").append("inteiro").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(SOMA_I_PATH + "/{valA}" + "/{valB}")
	public String getTextSoma(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("+").append(valB).append("=").append(valA+valB);
		return xml.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(SUBTRACAO_I_PATH + "/{valA}" + "/{valB}")
	public String getXmlSub(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("-").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA-valB).append("</RESULTADO>");
		xml.append("<NOME>").append("subtração").append("</NOME>");
		xml.append("<TIPO_DADO>").append("inteiro").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(SUBTRACAO_I_PATH + "/{valA}" + "/{valB}")
	public String getTextSub(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("-").append(valB).append("=").append(valA-valB);
		return xml.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(MULTIPLICACAO_I_PATH + "/{valA}" + "/{valB}")
	public String getXmlMul(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("*").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA*valB).append("</RESULTADO>");
		xml.append("<NOME>").append("multiplicação").append("</NOME>");
		xml.append("<TIPO_DADO>").append("inteiro").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(MULTIPLICACAO_I_PATH + "/{valA}" + "/{valB}")
	public String getTextMul(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("*").append(valB).append("=").append(valA*valB);
		return xml.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(DIVISAO_I_PATH + "/{valA}" + "/{valB}")
	public String getXmlDiv(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("/").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA/valB).append("</RESULTADO>");
		xml.append("<NOME>").append("divisão").append("</NOME>");
		xml.append("<TIPO_DADO>").append("inteiro").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(DIVISAO_I_PATH + "/{valA}" + "/{valB}")
	public String getTextDiv(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("/").append(valB).append("=").append(valA/valB);
		return xml.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(RESTO_I_PATH + "/{valA}" + "/{valB}")
	public String getXmlResto(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("%").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA%valB).append("</RESULTADO>");
		xml.append("<NOME>").append("resto").append("</NOME>");
		xml.append("<TIPO_DADO>").append("inteiro").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(RESTO_I_PATH + "/{valA}" + "/{valB}")
	public String getTextResto(@PathParam("valA") int valA, @PathParam("valB") int valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("%").append(valB).append("=").append(valA%valB);
		return xml.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(SOMA_PATH + "/{valA}" + "/{valB}")
	public String getXmlSoma(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("+").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA+valB).append("</RESULTADO>");
		xml.append("<NOME>").append("soma").append("</NOME>");
		xml.append("<TIPO_DADO>").append("ponto flutuante").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(SOMA_PATH + "/{valA}" + "/{valB}")
	public String getTextSoma(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("+").append(valB).append("=").append(valA+valB);
		return xml.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(SUBTRACAO_PATH + "/{valA}" + "/{valB}")
	public String getXmlSub(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("-").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA-valB).append("</RESULTADO>");
		xml.append("<NOME>").append("subtração").append("</NOME>");
		xml.append("<TIPO_DADO>").append("ponto flutuante").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(SUBTRACAO_PATH + "/{valA}" + "/{valB}")
	public String getTextSub(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("-").append(valB).append("=").append(valA-valB);
		return xml.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(MULTIPLICACAO_PATH + "/{valA}" + "/{valB}")
	public String getXmlMul(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("*").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA*valB).append("</RESULTADO>");
		xml.append("<NOME>").append("multiplicação").append("</NOME>");
		xml.append("<TIPO_DADO>").append("ponto flutuante").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(MULTIPLICACAO_PATH + "/{valA}" + "/{valB}")
	public String getTextMul(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("*").append(valB).append("=").append(valA*valB);
		return xml.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(DIVISAO_PATH + "/{valA}" + "/{valB}")
	public String getXmlDiv(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("/").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA/valB).append("</RESULTADO>");
		xml.append("<NOME>").append("divisão").append("</NOME>");
		xml.append("<TIPO_DADO>").append("ponto flutuante").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(DIVISAO_PATH + "/{valA}" + "/{valB}")
	public String getTextDiv(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("/").append(valB).append("=").append(valA/valB);
		return xml.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path(RESTO_PATH + "/{valA}" + "/{valB}")
	public String getXmlResto(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<OPERACAO>");
		xml.append("<OPERANDO1>").append(valA).append("</OPERANDO1>");
		xml.append("<OPERANDO2>").append(valB).append("</OPERANDO2>");
		xml.append("<OPERADOR>").append("%").append("</OPERADOR>");
		xml.append("<RESULTADO>").append(valA%valB).append("</RESULTADO>");
		xml.append("<NOME>").append("resto").append("</NOME>");
		xml.append("<TIPO_DADO>").append("ponto flutuante").append("</TIPO_DADO>");
		xml.append("</OPERACAO>");
		return xml.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path(RESTO_PATH + "/{valA}" + "/{valB}")
	public String getTextResto(@PathParam("valA") float valA, @PathParam("valB") float valB) {
		StringBuffer xml = new StringBuffer();
		xml.append(valA).append("%").append(valB).append("=").append(valA%valB);
		return xml.toString();
	}
}
